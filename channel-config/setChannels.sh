#!bin/bash

cd /etc/hyperledger/allorgs

export CORE_PEER_TLS_ENABLED=false
export GOPATH=/opt/gopath



# burris peer 1
export CORE_PEER_ID=peer1st-burris
export CORE_PEER_ADDRESS=peer1st-burris:7051
export CORE_PEER_LOCALMSPID=burris
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/burris/users/Admin@burris/msp

# channel creation
peer channel create -o orderer1st-orgo:7050 -c bjspurduechannel -f /etc/hyperledger/allorgs/bjspurduechannel.tx --timeout 120
peer channel create -o orderer1st-orgo:7050 -c bjschrobinsonchannel -f /etc/hyperledger/allorgs/bjschrobinsonchannel.tx --timeout 120
peer channel create -o orderer1st-orgo:7050 -c burrischannel -f /etc/hyperledger/allorgs/burrischannel.tx --timeout 120

peer channel join -b burrischannel.block
peer channel join -b bjspurduechannel.block
peer channel join -b bjschrobinsonchannel.block

# burris peer 2
export CORE_PEER_ID=peer2nd-burris
export CORE_PEER_ADDRESS=peer2nd-burris:7051
export CORE_PEER_LOCALMSPID=burris
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/burris/users/Admin@burris/msp

peer channel join -b burrischannel.block
peer channel join -b bjspurduechannel.block
peer channel join -b bjschrobinsonchannel.block


# bjs peer 1
export CORE_PEER_ID=peer1st-bjs
export CORE_PEER_ADDRESS=peer1st-bjs:7051
export CORE_PEER_LOCALMSPID=bjs
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/bjs/users/Admin@bjs/msp

peer channel join -b bjspurduechannel.block
peer channel join -b bjschrobinsonchannel.block

# bjs peer 2
export CORE_PEER_ID=peer2nd-bjs
export CORE_PEER_ADDRESS=peer2nd-bjs:7051
export CORE_PEER_LOCALMSPID=bjs
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/bjs/users/Admin@bjs/msp

peer channel join -b bjspurduechannel.block
peer channel join -b bjschrobinsonchannel.block

# purdue peer 1
export CORE_PEER_ID=peer1st-purdue
export CORE_PEER_ADDRESS=peer1st-purdue:7051
export CORE_PEER_LOCALMSPID=purdue
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/purdue/users/Admin@purdue/msp

peer channel join -b bjspurduechannel.block

# purdue peer 2
export CORE_PEER_ID=peer2nd-purdue
export CORE_PEER_ADDRESS=peer2nd-purdue:7051
export CORE_PEER_LOCALMSPID=purdue
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/purdue/users/Admin@purdue/msp

peer channel join -b bjspurduechannel.block


# chrobinson peer 1
export CORE_PEER_ID=peer1st-chrobinson
export CORE_PEER_ADDRESS=peer1st-chrobinson:7051
export CORE_PEER_LOCALMSPID=chrobinson
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/chrobinson/users/Admin@chrobinson/msp

peer channel join -b bjschrobinsonchannel.block

# chrobinson peer 2
export CORE_PEER_ID=peer2nd-chrobinson
export CORE_PEER_ADDRESS=peer2nd-chrobinson:7051
export CORE_PEER_LOCALMSPID=chrobinson
export CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/allorgs/chrobinson/users/Admin@chrobinson/msp

peer channel join -b bjschrobinsonchannel.block
