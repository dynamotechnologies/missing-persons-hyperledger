# Get latest docker images
docker pull hyperledger/composer-cli:latest 
docker pull hyperledger/composer-playground:latest 

# add public key and upload bna
cat ~/.ssh/id_rsa.pub
scp burris-network-2.bna ubuntu@18.188.180.151:/opt/gopath/burris_config/fabric/run/keyfiles/burris-network.bna

# Create comopser directory and give proper permissions
mkdir ~/.composer
sudo chown -R ubuntu ~/.composer

TODO:
- make policy file for each of the business networks
	- bjspurduechannel
	- bjschrobinsonchannel
	- burrischannel
- make connection file for each of the business networks and orgs


# Create PeerAdmin card

docker run -v /home/ubuntu/.composer:/home/composer/.composer
-v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric
hyperledger/composer-cli:latest card create
-p /opt/gopath/burris_config/fabric/run/keyfiles/burris/connection.json
-c /opt/gopath/burris_config/fabric/run/keyfiles/burris/users/Admin@burris/msp/admincerts/Admin@burris-cert.pem
-k /opt/gopath/burris_config/fabric/run/keyfiles/burris/users/Admin@burris/msp/keystore/admin_private.key
-r PeerAdmin -r ChannelAdmin
-u PeerAdmin@burris
-f /opt/gopath/burris_config/fabric/run/keyfiles/burris/burris-PeerAdmin.card

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest card create -p /opt/gopath/burris_config/fabric/run/keyfiles/burris/connection.json -c /opt/gopath/burris_config/fabric/run/keyfiles/burris/users/Admin@burris/msp/admincerts/Admin@burris-cert.pem -k /opt/gopath/burris_config/fabric/run/keyfiles/burris/users/Admin@burris/msp/keystore/admin_private.key -r PeerAdmin -r ChannelAdmin -u PeerAdmin -f /opt/gopath/burris_config/fabric/run/keyfiles/burris/burris-PeerAdmin.card



# Import PeerAdmin card

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest card import -f /opt/gopath/burris_config/fabric/run/keyfiles/burris/burris-PeerAdmin.card


## delete
docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest card delete -c PeerAdmin@burris@burris-network



# Install composer runtime on to peer nodes for Burris
# use burris-network-2

docker run -v {{ localhome }}:/home/composer/.composer
-v {{ fabricworkdir }}:{{ fabricworkdir }}
hyperledger/composer-cli:next runtime install
-c PeerAdmin@{{ item }} -n {{ networkname }}

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network install --card PeerAdmin@burris-network --archiveFile /opt/gopath/burris_config/fabric/run/keyfiles/burris-network.bna 


# 2
docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network install --card PeerAdmin@burris --archiveFile /opt/gopath/burris_config/fabric/run/keyfiles/burris-network.bna 

# Successfully installed business network burris-network, version 0.0.1


# Request Identity
# composer identity request -c PeerAdmin@burris -u admin -s adminpw -d burrisAdmin

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest identity request -c PeerAdmin@burris-network -u admin -s adminpw -d /home/composer/.composer/burrisAdmin


# Start network

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network start -c PeerAdmin@burris -n burris-network -V 0.0.1 -A /home/composer/.composer/burrisAdmin -C /home/composer/.composer/burrisAdmin/admin-pub.pem

# with endorsement
docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network start -c PeerAdmin@burris-network -o endorsementPolicyFile=/opt/gopath/burris_config/fabric/run/keyfiles/endorsement-policy.json -n burris-network -V 0.0.1 -A /home/composer/.composer/burrisAdmin -C /home/composer/.composer/burrisAdmin/admin-pub.pem


# used this
docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network start -c PeerAdmin@burris-network -o endorsementPolicyFile=/opt/gopath/burris_config/fabric/run/keyfiles/endorsement-policy.json -n burris-network -V 0.0.1 -A burrisAdmin -C /home/composer/.composer/burrisAdmin/admin-pub.pem




# Create business network card

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest card create -p /opt/gopath/burris_config/fabric/run/keyfiles/burris/connection.json -u burrisAdmin -n burris-network -c /home/composer/.composer/burrisAdmin/admin-pub.pem -k /home/composer/.composer/burrisAdmin/admin-priv.pem -f /home/composer/.composer/burrisAdmin@burris-network.card


# 	Output file: burrisAdmin@burris-network.card



docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest card import --file /home/composer/.composer/burrisAdmin@burris-network.card 

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric hyperledger/composer-cli:latest network ping -c burrisAdmin@burris-network


# Start playground

docker run -v /home/ubuntu/.composer:/home/composer/.composer -v /opt/gopath/burris_config/fabric:/opt/gopath/burris_config/fabric --name composer-playground --publish 8080:8080 --detach hyperledger/composer-playground:latest